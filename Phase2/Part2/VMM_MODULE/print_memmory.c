#include <linux/module.h>    
#include <linux/kernel.h>    
#include <linux/init.h>      
#include <linux/sched.h>     
#include <linux/proc_fs.h>
#include <linux/kthread.h>
#include <linux/delay.h>
static struct task_struct *task_thread;
static pid_t thread_id;
static int prev[20000] = {0};
int print_memmory(void *unused_data)
{
	allow_signal(SIGKILL);
	while(!kthread_should_stop()){
		struct task_struct* task;
       		for_each_process(task) {
               	        if(task->mm != NULL && task->pid < 20000){
				int diff = task->mm->total_vm - prev[task->pid];
				prev[task->pid] = task->mm->total_vm;
				if (diff > 0)                      		 
					printk(KERN_INFO "TOTAL VM ALLOCATED: [%d] <=====> PID : [%d]\n", diff, task->pid);
				else if (diff < 0)
					printk(KERN_INFO "TOTAL VM FREED: [%d] <=====> PID : [%d]\n", -1*diff, task->pid);}}
		ssleep(3);
		if (signal_pending(task_thread)){break;}}
        return 0;}
static int __init print_memmory_init(void) {     
    	printk(KERN_INFO "===> PRINTMEM MODULE INITED\n");
        task_thread = kthread_run(&print_memmory, NULL, "");
	if (task_thread){thread_id = task_thread->pid;}
	else {thread_id = 0;}
 	return 0;}
static void __exit print_memmory_cleanup(void) {
	kthread_stop(task_thread);
	printk(KERN_INFO "===> PRINTMEM MODULE INITED");}
module_init(print_memmory_init);     
module_exit(print_memmory_cleanup); 
