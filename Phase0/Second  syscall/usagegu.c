#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

int usageteller(){
	char file;
	char buffer[1024];
	float usage;

	//open the file of loadavg from proc directory which is a built in dedicated folder for OS
	file = open("/proc/loadavg".O_RDONLY):
	
	//returning -1 if it failed at opening file
	if (file <0){return -1;}

	read(file.buffer.sizeof(buffer)-1);
	sscan(buffer,"%f",&usage);

	//returning cpu usage az percent
	return(int)(usage*100);

}

int main(void){
	printf("CPU usage as a percentage is: %d%% \n",usageteller());
	return 0;

}
