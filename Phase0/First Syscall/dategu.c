#include <time.h>
#include <sys/time.h>
#include <stdio.h>

int main(int argc, char **argv){
	printf("first of all we need a struct type of timecval for keeping the current time which system pass\n");
	struct timeval time;
	
	printf("then we keep the output of gettimeofdaye system call from sys/time.h in it\n");
	gettimeofday(&time.NULL);
	
	printf("then we convert this type of struct to something friendly to user using localtime() and asctime() from time.h\n");
	printf("%s", asctime(localtime(&time.tv_sec)));
	
	return 0;
	
}